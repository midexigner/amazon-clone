

- setup react app using npx create-react-app amazon-clone
- setup firebase
    - npm install -g firebase-tools
- setup react router
    - npm install react-router-dom
- build react navbar (header)
    - npm install @material-ui/core
    - npm install @material-ui/icons
- build the banner
- build the product component
- introduce react context API
    - create `StateProvider.js` file

```sh
// setup data layer
// we need this to track the basket
import React, { createContext,useContext ,useReducer } from "react";

// this is the data layer
export const StateContext = createContext();

// build a provider
export const StateProvider = ({reducer,initialState,children})=>(
    <StateContext.Provider value={useReducer(reducer,initialState)}>
        {children}
    </StateContext.Provider>
)

// this is how we use it inside of a component
export const useStateValue = () => useContext(StateContext);
```

    - create `Reducer.js` file

```sh
export const initialState = {
    basket:[],
    user:null
}

const reducer = (state,action)=>{
    console.log(action);
    switch(action.type){
        case 'ADD_TO_BASKET':
        // logic for adding item to basket
        return {
            ...state,
            basket: [...state.basket, action.item],
          };
        break;
        case 'EMPTY_BASKET':
        // logic for Removing item to basket
        return {
            ...state,
            basket:[]
        };
        case 'REMOVE_FROM_BASKET':
        // logic for Removing item to basket
        let newBasket = [...state.basket];
        const index = state.basket.findIndex( 
            (basketItem) => basketItem.id === action.id)
            if (index >= 0) {
                newBasket.splice(index, 1);
        
              } else {
                console.warn(
                  `Cant remove product (id: ${action.id}) as its not in basket!`
                )
              }
        return {
            ...state,
            basket: newBasket
        };
        break;
        default:
          return state;
    }
}
export default reducer;



```

- npm i react-currency-format