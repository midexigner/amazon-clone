import React from "react";
import "./Checkout.css";
import { useStateValue } from "../../StateProvider";
import CheckoutProduct from "../../components/CheckoutProduct/CheckoutProduct"
import Subtotal from "../../components/Subtotal/Subtotal";

const Checkout = ()=> {
    const [{ basket }, dispatch] = useStateValue();

    return (
        <div className="checkout">
           <div className="checkout__left">
        <img
          className="checkout__ad"
          src="https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_.jpg"
          alt=""
        />

        {basket?.length === 0 ?(
     <div>
          <h2 className="checkout__title">Your shopping Basket is empty</h2>
          <p>You have no items in your basket. To buy one or "<strong>Add to Basket</strong>" next to the item</p>
        </div>
        ) : (
            <div>
                 <h2 className="checkout__title">Your shopping Basket</h2>
               {basket.map((item,index) => (
      <CheckoutProduct
      key={index}
              id={item.id}
              title={item.title}
              image={item.image}
              price={item.price}
              rating={item.rating}
            />
          ))}
            </div>
        )}
        </div>
<div className="checkout__right">
       <Subtotal /> 

       
      </div>
     
    </div>
    )
}

export default Checkout
